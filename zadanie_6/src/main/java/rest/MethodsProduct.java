package rest;


import Basic.Product;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mateuszszczukowski on 23/05/2017.
 */
@Stateless
public class MethodsProduct {

	@PersistenceContext
	EntityManager em;
	
	
    // DODAWANIE PRODUKTU
    public Response add(Product product) {
    	em.persist(product);
    	return Response.ok(product.getID()).build();
    }

    // WYŚWIETLANIE LISTY WSZYSTKICH PRODUKTÓW
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Product> showAllProducts()
    {
    	return em.createNamedQuery("ProductAll", Product.class).getResultList();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    // WYŚWIETLANIE PRODUKTU O DANYM ID
    public Response showTheProduct(@PathParam("id") int id)
    {
    	Product p = em.createNamedQuery("Product_ID",Product.class).setParameter("Id",id).getSingleResult();
    	if(p == null)
    		return Response.status(404).build();
    	return Response.ok(p).build();
    }
//a
    // AKTUALIZACJA PRODUKTU
    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response editProduct(@PathParam("id") int id, Product p) {

        /*Product result = db.get(id);
        if (result == null)
            return Response.status(404).build();
        p.setID();
        p.setName();
        p.setType()
        p.setDescription();
        p.setPrice();

        db.editProduct(p);*/ 
        return Response.ok().build();

    }

    // WYSZUKIWANIE PRODUKTU PO CENIE
    public void searchPriceProduct() {}

    // WYSZUKIWANIE PRODUKTU PO NAZWIE
    public void searchNameProduct() {}

    // WYSZUKIWANIE PRODUKTU PO Kategorii
    @GET
    @Path("/Search/{type}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Product> searchCategoryProduct(@PathParam("type") int type) 
    {
    	return em.createNamedQuery("SearchProductType" , Product.class).setParameter("type",type).getResultList();
    }

    // DODAWANIE KOMENTARZY O DANYM PRODUKCIE
    public void addCommentProduct() {}

    // USUWANIE KOMENTARZY
    public void delCommentProduct() {}

    // WYŚWIETLANIE KOMENTARZY
    public void showAllCommentProduct() {}
}
