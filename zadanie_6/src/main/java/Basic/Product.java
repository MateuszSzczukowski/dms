package Basic;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * Test
 * Created by mateuszszczukowski on 23/05/2017.
 */

/**
 * 
 * @author shymek lol
 * Updated by szymonpolak on 25/05/2017.
 */

@XmlRootElement
@Entity
@NamedQueries({
	@NamedQuery(name ="ProductAll", query="Select p  From Product p"),
	@NamedQuery(name ="Product_ID", query="Select p  From Product p WHERE p.id = :Id"),
	@NamedQuery(name ="searchProductType", query="Select p From Product p WHERE p.type = :type")
})
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;
    private String name;
    private int type;
    private String description;
    private float price;

    public Product(int ID, String name, int type, String description, float price) {
        this.ID = ID;
        this.name = name;
        this.type = type;
        this.description = description;
        this.price = price;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}
